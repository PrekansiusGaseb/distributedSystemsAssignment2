import ballerina/graphql;
import ballerina/io;
import ballerina/sql;
import ballerinax/mysql;
import ballerinax/mysql.driver as _;

configurable int port = 9090;

service /graphql on new graphql:Listener(port) {

    private final mysql:Client db;
    function init() returns error? {
        self.db = check new ("localhost", "root", "", "dsa_ass_2", 3306);
    }

    resource function get getUserById(string id) returns User? {
        io:println("Get User Touched !");
        io:println(id);
        sql:ParameterizedQuery query = `SELECT * FROM User WHERE id  = ${id}}`;
        io:println(query);
        do {
            var result = self.db->query(query, UserInput);
            io:println(result);
            if (result is stream<UserInput>) {
                io:println(result.first());
            }
        } on fail var e {
            io:println(e);
            return;
        }
        return;
    }

    resource function get getUsersByRole(string role) returns User?[]? {
    sql:ExecutionResult|sql:Error result = self.db->execute(`SELECT * FROM User WHERE role = ${role}`);
        if (result is sql:ExecutionResult) {
            return;
        } else {
            return;
        }
    }

    resource function get getKPIById(string id) returns KPI? {
    sql:ExecutionResult|sql:Error result = self.db->execute(`SELECT * FROM KPI WHERE employeeId = ${id}`);
        if (result is sql:ExecutionResult) {
            return;
        } else {
            return;
        }
    }

    resource function get getEmployeeKPIs(string employeeId) returns EmployeeKPI?[]? {
    sql:ExecutionResult|sql:Error result = self.db->execute(`SELECT * FROM EmployeeKPI WHERE employeeId = ${employeeId}`);
        if (result is sql:ExecutionResult) {
            return;
        } else {
            return;
        }
    }

    resource function get getDepartmentObjectives(string employeeId) returns DepartmentObjective?[]? {
    io:println("Get Department Objectives Touched !");
    sql:ExecutionResult|sql:Error result = self.db->execute(`SELECT * FROM DepartmentObjective WHERE employeeId = ${employeeId}`);
        if (result is sql:ExecutionResult) {
            return;
        } else {
            return;
        }
    }

    remote function createUser(UserInput user) returns User? {
        io:println("Create User Touched !");
        sql:ExecutionResult|sql:Error result = self.db->execute(`INSERT INTO TableName (username, password, role, department, position, email,
                                     assignedSupervisor, assignedHoD, assignedEmployees, objectives,
                                     grades, KPIs) VALUES 
                                     ('${user.username}', '${user.password}', '${user.role}', 
                                      '${user.department}', '${user.position}', '${user.email}', 
                                      '${user.assignedSupervisor}', '${user.assignedHoD}', 
                                      '{${user.assignedEmployees}}', '${user.objectives}', 
                                      '${user.grades.toJsonString()}', '${user.KPIs}')`);

        if (result is sql:ExecutionResult) {
            return;
        } else {
            return;
        }
    }

    remote function updateUser(string id, UserInput user) returns User? {
        io:println("Update User Touched !");
        sql:ExecutionResult|sql:Error result = self.db->execute(`UPDATE User 
                                     SET username = '${user.username}', 
                                         password = '${user.password}', 
                                         role = '${user.role}', 
                                         department = '${user.department}', 
                                         position = '${user.position}', 
                                         email = '${user.email}', 
                                         assignedSupervisor = '${user.assignedSupervisor}', 
                                         assignedHoD = '${user.assignedHoD}', 
                                         assignedEmployees = '{${user.assignedEmployees}}', 
                                         objectives = '${user.objectives}', 
                                         grades = '${user.grades.toJsonString()}', 
                                         KPIs = '${user.KPIs}' 
                                     WHERE user_id = '${id}'`);

        if (result is sql:ExecutionResult) {
            return;
        } else {
            return;
        }
    }

    remote function deleteUser(string id) returns User? {
        io:println("Delete User Touched !");
        sql:ExecutionResult|sql:Error result = self.db->execute(`DELETE FROM User WHERE id = ${id}`);
        if (result is sql:ExecutionResult) {
            return;
        } else {
            return ();
        }
    }

    remote function createKPI(KPIInput kpi) returns KPI? {
        io:println("createKPI User Touched !");
        sql:ExecutionResult|sql:Error result = self.db->execute(`INSERT INTO KPI (name, objective,unitweightage,createdBy,createdAt) VALUES ('${kpi.name}', '${kpi.objective}','${kpi.unit}','${kpi.createdBy}','${kpi.createdAt}')`);
        if (result is sql:ExecutionResult) {
            return;
        } else {
            return ();
        }
    }

    remote function updateKPI(string id, KPIInput kpi) returns KPI? {
        io:println("Delete User Touched !");
        sql:ExecutionResult|sql:Error result = self.db->execute(`UPDATE KPI
                                     SET name = '${kpi.name}', 
                                     SET objective = '${kpi.objective}', 
                                     SET unit = '${kpi.unit}', 
                                     SET weightage = '${kpi.weightage}', 
                                     SET createdBy = '${kpi.createdBy}', 
                                     SET createdAt = '${kpi.createdAt}', 
                                     WHERE kpi_id = '${id}'`);
        if (result is sql:ExecutionResult) {
            return;
        } else {
            return ();
        }
    }
    remote function deleteKPI(string id) returns KPI? {
        io:println("Delete User Touched !");
        sql:ExecutionResult|sql:Error result = self.db->execute(`DELETE FROM User WHERE id = ${id}`);
        if (result is sql:ExecutionResult) {
            return;
        } else {
            return ();
        }
    }

    remote function createEmployeeKPI(EmployeeKPIInput employeeKPI) returns EmployeeKPI? {
        io:println("Delete User Touched !");
        sql:ExecutionResult|sql:Error result = self.db->execute(`INSERT INTO EmployeeKPI (employeeId,kpiId,value,gradedBy,grade,evalDate)
                                                             VALUES ('${employeeKPI.employeeId}', ${employeeKPI.kpiId}', ${employeeKPI.value}', ${employeeKPI.gradedBy}', ${employeeKPI.grade}', ${employeeKPI.evalDate}'`);
        if (result is sql:ExecutionResult) {
            return;
        } else {
            return ();
        }
    }

    remote function createDepartmentObjective(DepartmentObjectiveInput departmentObjective) returns DepartmentObjective? {
        io:println("Delete User Touched !");
        sql:ExecutionResult|sql:Error result = self.db->execute(`INSERT INTO DepartmentObjective (employeeId,kpiId,value,gradedBy,grade,evalDate)
                                                             VALUES (
                                                                     '${departmentObjective.employeeId}',
                                                                     '${departmentObjective.kpiId}',
                                                                     '${departmentObjective.value}',
                                                                     '${departmentObjective.gradedBy}',
                                                                     '${departmentObjective.grade}',
                                                                     '${departmentObjective.evalDate}')`);

        if (result is sql:ExecutionResult) {
            return;
        } else {
            return ();
        }
    }
}

type User record {|
        string employeeId;
    string kpiId;
    int value;
    string gradedBy;
    int grade;
    string evalDate;
|};

type KPI record {|
        string employeeId;
    string kpiId;
    int value;
    string gradedBy;
    int grade;
    string evalDate;
|};

type EmployeeKPI record {|
        string employeeId;
    string kpiId;
    int value;
    string gradedBy;
    int grade;
    string evalDate;
|};

type DepartmentObjective record {|
        string employeeId;
    string kpiId;
    int value;
    string gradedBy;
    int grade;
    string evalDate;
|};

type graphql service object {
    *graphql:Service;
    resource function get getUserById(string id) returns User?;
    resource function get getUsersByRole(string role) returns User?[]?;
    resource function get getKPIById(string id) returns KPI?;
    resource function get getEmployeeKPIs(string employeeId) returns EmployeeKPI?[]?;
    resource function get getDepartmentObjectives(string employeeId) returns DepartmentObjective?[]?;
    remote function createUser(UserInput user) returns User?;
    remote function updateUser(string id, UserInput user) returns User?;
    remote function deleteUser(string id) returns User?;
    remote function createKPI(KPIInput kpi) returns KPI?;
    remote function updateKPI(string id, KPIInput kpi) returns KPI?;
    remote function deleteKPI(string id) returns KPI?;
    remote function createEmployeeKPI(EmployeeKPIInput employeeKPI) returns EmployeeKPI?;
    remote function createDepartmentObjective(DepartmentObjectiveInput departmentObjective) returns DepartmentObjective?;
};

public type DepartmentObjectiveInput record {|
    string employeeId;
    string kpiId;
    int value;
    string gradedBy;
    int grade;
    string evalDate;
|};

public type EmployeeKPIInput record {|
    string employeeId;
    string kpiId;
    int value;
    string gradedBy;
    int grade;
    string evalDate;
|};

public type GradeInput record {|
    string employeeId;
    int grade;
|};

public type KPIInput record {|
    string name;
    string objective;
    string unit;
    float weightage;
    string createdBy;
    string createdAt;
|};

public type UserInput record {|
    string username;
    string password;
    string role;
    string department;
    string position;
    string email;
    string? assignedSupervisor;
    string? assignedHoD;
    string?[]? assignedEmployees;
    string?[]? objectives;
    GradeInput?[]? grades;
    string?[]? KPIs;
|};
