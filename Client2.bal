import ballerina/io;
import ballerina/graphql;

type UserResponse record {|
    record {|UserInput profile;|} data;
|};

 public function main() returns error? {
    graphql:Client graphqlClient = check new ("localhost:9090/graphql"); 

    string getUserByIdQuery = "query { getUserById(id: \"1\") {employeeId, kpiId, value, gradedBy, grade, evalDate } }";
    string getUsersByRoleQuery = "query { getUsersByRole(role: \"yourRole\") {employeeId, kpiId, value, gradedBy, grade, evalDate } }";
    string getKPIByIdQuery = "query { getKPIById(id: \"1\") {name, objective, unit, weightage, createdBy, createdAt } }";
    string getEmployeeKPIsQuery = "query { getEmployeeKPIs(employeeId: \"1\") {employeeId, kpiId, value, gradedBy, grade, evalDate } }";
    string getDepartmentObjectivesQuery = "query { getDepartmentObjectives(employeeId: \"1\") {employeeId, kpiId, value, gradedBy, grade, evalDate } }";
    string createUserMutation = "mutation { createUser(user: { username: \"yourUsername\", password: \"yourPassword\", role: \"yourRole\", department: \"yourDepartment\", position: \"yourPosition\", email: \"yourEmail\" }) { employeeId, kpiId, value, gradedBy, grade, evalDate } }";
    string updateUserMutation = "mutation { updateUser(id: \"1\", user: { username: \"yourUsername\", password: \"yourPassword\", role: \"yourRole\", department: \"yourDepartment\", position: \"yourPosition\", email: \"yourEmail\" }) { employeeId, kpiId, value, gradedBy, grade, evalDate } }";
    string deleteUserMutation = "mutation { deleteUser(id: \"1\") { employeeId, kpiId, value, gradedBy, grade, evalDate } }";
    string createKPIMutation = "mutation { createKPI(kpi: { name: \"yourKPIName\", objective: \"yourObjective\", unit: \"yourUnit\", createdBy: \"yourCreatedBy\", createdAt: \"yourCreatedAt\" }) { employeeId, kpiId, value, gradedBy, grade, evalDate } }";
    string updateKPIMutation = "mutation { updateKPI(id: \"1\", kpi: { name: \"yourKPIName\", objective: \"yourObjective\", unit: \"yourUnit\", createdBy: \"yourCreatedBy\", createdAt: \"yourCreatedAt\" }) { employeeId, kpiId, value, gradedBy, grade, evalDate } }";
    string deleteKPIMutation = "mutation { deleteKPI(id: \"1\") { employeeId, kpiId, value, gradedBy, grade, evalDate } }";
    string createEmployeeKPIMutation = "mutation { createEmployeeKPI(employeeKPI: { employeeId: \"yourEmployeeId\", kpiId: \"yourKPIId\", value: 42, gradedBy: \"yourGrader\", grade: 5, evalDate: \"yourEvalDate\" }) { employeeId, kpiId, value, gradedBy, grade, evalDate } }";
    string createDepartmentObjectiveMutation = "mutation { createDepartmentObjective(departmentObjective: { employeeId: \"yourEmployeeId\", kpiId: \"yourKPIId\", value: 42, gradedBy: \"yourGrader\", grade: 5, evalDate: \"yourEvalDate\" }) { employeeId, kpiId, value, gradedBy, grade, evalDate } }";
    
    UserResponse _ = check graphqlClient->execute(getUserByIdQuery);
    UserResponse _ = check graphqlClient->execute(getUsersByRoleQuery);
    UserResponse _ = check graphqlClient->execute(getKPIByIdQuery);
    UserResponse _ = check graphqlClient->execute(getEmployeeKPIsQuery);
    UserResponse _ = check graphqlClient->execute(getDepartmentObjectivesQuery);
    UserResponse _ = check graphqlClient->execute(createUserMutation);
    UserResponse _ = check graphqlClient->execute(updateUserMutation);
    UserResponse _ = check graphqlClient->execute(deleteUserMutation);
    UserResponse _ = check graphqlClient->execute(createKPIMutation);
    UserResponse _ = check graphqlClient->execute(updateKPIMutation);
    UserResponse _ = check graphqlClient->execute(deleteKPIMutation);
    UserResponse _ = check graphqlClient->execute(createEmployeeKPIMutation);
    UserResponse _ = check graphqlClient->execute(createDepartmentObjectiveMutation);
    
    io:println("Complete !");
}

type User record {|
        string employeeId;
    string kpiId;
    int value;
    string gradedBy;
    int grade;
    string evalDate;
|};

type KPI record {|
        string employeeId;
    string kpiId;
    int value;
    string gradedBy;
    int grade;
    string evalDate;
|};

type EmployeeKPI record {|
        string employeeId;
    string kpiId;
    int value;
    string gradedBy;
    int grade;
    string evalDate;
|};

type DepartmentObjective record {|
        string employeeId;
    string kpiId;
    int value;
    string gradedBy;
    int grade;
    string evalDate;
|};

type graphql service object {
    *graphql:Service;
    resource function get getUserById(string id) returns User?;
    resource function get getUsersByRole(string role) returns User?[]?;
    resource function get getKPIById(string id) returns KPI?;
    resource function get getEmployeeKPIs(string employeeId) returns EmployeeKPI?[]?;
    resource function get getDepartmentObjectives(string employeeId) returns DepartmentObjective?[]?;
    remote function createUser(UserInput user) returns User?;
    remote function updateUser(string id, UserInput user) returns User?;
    remote function deleteUser(string id) returns User?;
    remote function createKPI(KPIInput kpi) returns KPI?;
    remote function updateKPI(string id, KPIInput kpi) returns KPI?;
    remote function deleteKPI(string id) returns KPI?;
    remote function createEmployeeKPI(EmployeeKPIInput employeeKPI) returns EmployeeKPI?;
    remote function createDepartmentObjective(DepartmentObjectiveInput departmentObjective) returns DepartmentObjective?;
};

public type DepartmentObjectiveInput record {|
    string employeeId;
    string kpiId;
    int value;
    string gradedBy;
    int grade;
    string evalDate;
|};

public type EmployeeKPIInput record {|
    string employeeId;
    string kpiId;
    int value;
    string gradedBy;
    int grade;
    string evalDate;
|};

public type GradeInput record {|
    string employeeId;
    int grade;
|};

public type KPIInput record {|
    string name;
    string objective;
    string unit;
    float weightage;
    string createdBy;
    string createdAt;
|};

public type UserInput record {|
    string username;
    string password;
    string role;
    string department;
    string position;
    string email;
    string? assignedSupervisor;
    string? assignedHoD;
    string?[]? assignedEmployees;
    string?[]? objectives;
    GradeInput?[]? grades;
    string?[]? KPIs;
|};
