import ballerina/io;
import ballerina/graphql;

public type User record {|
    string employeeId;
    string kpiId;
    int value;
    string gradedBy;
    int grade;
    string evalDate;
|};

public type KPI record {|
    string employeeId;
    string kpiId;
    int value;
    string gradedBy;
    int grade;
    string evalDate;
|};

public type EmployeeKPI record {|
    string employeeId;
    string kpiId;
    int value;
    string gradedBy;
    int grade;
    string evalDate;
|};

public type DepartmentObjective record {|
    string employeeId;
    string kpiId;
    int value;
    string gradedBy;
    int grade;
    string evalDate;
|};

public type GradeInput record {|
    string employeeId;
    int grade;
|};

public type KPIInput record {|
    string name;
    string objective;
    string unit;
    float weightage;
    string createdBy;
    string createdAt;
|};

public type UserInput record {|
    string username;
    string password;
    string role;
    string department;
    string position;
    string email;
    string? assignedSupervisor;
    string? assignedHoD;
    string?[]? assignedEmployees;
    string?[]? objectives;
    GradeInput?[]? grades;
    string?[]? KPIs;
|};

public type UserResponse record {|
    record {| User profile; |} data;
|};

public type KPIResponse record {|
    record {| KPI profile; |} data;
|};

public type EmployeeKPIResponse record {|
    record {| EmployeeKPI profile; |} data;
|};

public type DepartmentObjectiveResponse record {|
    record {| DepartmentObjective profile; |} data;
|};

public function main() returns error? {
    graphql:Client graphqlClient = check new ("localhost:9090/graphql"); 

    // Queries
    UserResponse getUserByIdResponse = check graphqlClient->execute("query { getUserById(id: \"1\") {employeeId, kpiId, value, gradedBy, grade, evalDate } }");
    UserResponse getUsersByRoleResponse = check graphqlClient->execute("query { getUsersByRole(role: \"yourRole\") {employeeId, kpiId, value, gradedBy, grade, evalDate } }");
    KPIResponse getKPIByIdResponse = check graphqlClient->execute("query { getKPIById(id: \"1\") {name, objective, unit, weightage, createdBy, createdAt } }");
    EmployeeKPIResponse getEmployeeKPIsResponse = check graphqlClient->execute("query { getEmployeeKPIs(employeeId: \"1\") {employeeId, kpiId, value, gradedBy, grade, evalDate } }");
    DepartmentObjectiveResponse getDepartmentObjectivesResponse = check graphqlClient->execute("query { getDepartmentObjectives(employeeId: \"1\") {employeeId, kpiId, value, gradedBy, grade, evalDate } }");

    // Mutations
    UserResponse createUserResponse = check graphqlClient->execute("mutation { createUser(user: { username: \"yourUsername\", password: \"yourPassword\", role: \"yourRole\", department: \"yourDepartment\", position: \"yourPosition\", email: \"yourEmail\" }) { employeeId, kpiId, value, gradedBy, grade, evalDate } }");
    UserResponse updateUserResponse = check graphqlClient->execute("mutation { updateUser(id: \"1\", user: { username: \"yourUsername\", password: \"yourPassword\", role: \"yourRole\", department: \"yourDepartment\", position: \"yourPosition\", email: \"yourEmail\" }) { employeeId, kpiId, value, gradedBy, grade, evalDate } }");
    UserResponse deleteUserResponse = check graphqlClient->execute("mutation { deleteUser(id: \"1\") { employeeId, kpiId, value, gradedBy, grade, evalDate } }");
    KPIResponse createKPIResponse = check graphqlClient->execute("mutation { createKPI(kpi: { name: \"yourKPIName\", objective: \"yourObjective\", unit: \"yourUnit\", createdBy: \"yourCreatedBy\", createdAt: \"yourCreatedAt\" }) { employeeId, kpiId, value, gradedBy, grade, evalDate } }");
    KPIResponse updateKPIResponse = check graphqlClient->execute("mutation { updateKPI(id: \"1\", kpi: { name: \"yourKPIName\", objective: \"yourObjective\", unit: \"yourUnit\", createdBy: \"yourCreatedBy\", createdAt: \"yourCreatedAt\" }) { employeeId, kpiId, value, gradedBy, grade, evalDate } }");
    KPIResponse deleteKPIResponse = check graphqlClient->execute("mutation { deleteKPI(id: \"1\") { employeeId, kpiId, value, gradedBy, grade, evalDate } }");
    EmployeeKPIResponse createEmployeeKPIMutationResponse = check graphqlClient->execute("mutation { createEmployeeKPI(employeeKPI: { employeeId: \"yourEmployeeId\", kpiId: \"yourKPIId\", value: 42, gradedBy: \"yourGrader\", grade: 5, evalDate: \"yourEvalDate\" }) { employeeId, kpiId, value, gradedBy, grade, evalDate } }");
    DepartmentObjectiveResponse createDepartmentObjectiveResponse = check graphqlClient->execute("mutation { createDepartmentObjective(departmentObjective: { employeeId: \"yourEmployeeId\", kpiId: \"yourKPIId\", value: 42, gradedBy: \"yourGrader\", grade: 5, evalDate: \"yourEvalDate\" }) { employeeId, kpiId, value, gradedBy, grade, evalDate } }");
    
    io:println("Complete !");
}


type graphql service object {
    *graphql:Service;
    resource function get getUserById(string id) returns User?;
    resource function get getUsersByRole(string role) returns User?[]?;
    resource function get getKPIById(string id) returns KPI?;
    resource function get getEmployeeKPIs(string employeeId) returns EmployeeKPI?[]?;
    resource function get getDepartmentObjectives(string employeeId) returns DepartmentObjective?[]?;
    remote function createUser(UserInput user) returns User?;
    remote function updateUser(string id, UserInput user) returns User?;
    remote function deleteUser(string id) returns User?;
    remote function createKPI(KPIInput kpi) returns KPI?;
    remote function updateKPI(string id, KPIInput kpi) returns KPI?;
    remote function deleteKPI(string id) returns KPI?;
    remote function createEmployeeKPI(EmployeeKPIInput employeeKPI) returns EmployeeKPI?;
    remote function createDepartmentObjective(DepartmentObjectiveInput departmentObjective) returns DepartmentObjective?;
};

public type DepartmentObjectiveInput record {|
    string employeeId;
    string kpiId;
    int value;
    string gradedBy;
    int grade;
    string evalDate;
|};

public type EmployeeKPIInput record {|
    string employeeId;
    string kpiId;
    int value;
    string gradedBy;
    int grade;
    string evalDate;
|};

public type GradeInput record {|
    string employeeId;
    int grade;
|};

public type KPIInput record {|
    string name;
    string objective;
    string unit;
    float weightage;
    string createdBy;
    string createdAt;
|};

public type UserInput record {|
    string username;
    string password;
    string role;
    string department;
    string position;
    string email;
    string? assignedSupervisor;
    string? assignedHoD;
    string?[]? assignedEmployees;
    string?[]? objectives;
    GradeInput?[]? grades;
    string?[]? KPIs;
|};
